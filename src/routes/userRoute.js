const express = require("express");
const userRoute = express.Router();
const { checkToken } = require("../controllers/authController");

//import controller
const {
  login,
  singUp,
  getUser,
  createUser,
  updateUser,
} = require("../controllers/userController");

// login
userRoute.post("/login", login);

// singup
userRoute.post("/singup", singUp);

// localhost:8080/api/user/getUser
//GET
userRoute.get("/getUser", getUser);

//POST
userRoute.post("/createUser", createUser);

//PUT
userRoute.put("/updateUser/:user_id", checkToken, updateUser);

//DELETE

module.exports = userRoute;

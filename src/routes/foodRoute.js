const express = require("express");
const { checkToken } = require("../controllers/authController");
const foodRoute = express.Router();

// import controller xử lý food
const {
  getFood,
  uploadFood,
  likeRestaurent,
  getLikeRestaurent,
} = require("../controllers/foodController");

const upload = require("../controllers/uploadControler");
//object literal

// upload
foodRoute.post("/upload", checkToken, upload.single("data"), uploadFood);

foodRoute.get("/getFood", getFood);

foodRoute.post("/like-restaurent/:res_id", checkToken, likeRestaurent);

foodRoute.get("/like-restaurent/:res_id", checkToken, getLikeRestaurent);

module.exports = foodRoute;

const express = require('express');
const foodRoute = require('./foodRoute');
const rootRoute = express.Router();


// import đối tượng router
const userRoute = require('./userRoute');

// localhost:8080/api/user/getUser
rootRoute.use("/user", userRoute);

// localhost:8080/api/food/getFood
rootRoute.use("/food", foodRoute);

//productRoute
//rootRoute.use("/product",productRoute)

module.exports = rootRoute;

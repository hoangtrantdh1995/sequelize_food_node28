// authentication => xác minh user

const { verifyToken, decodeToken } = require("../config/jwt");

// authorization => ủy quyền user ( phân quyền)

const checkToken = (req, res, next) => {
  try {
    let { token } = req.headers;
    if (verifyToken(token)) {
      // token hợp lệ
      const userInfo = decodeToken(token);
      res.locals = {
        user: userInfo.data,
      };
      next();
    }

    // verifyToken

    // checkToken
    // true => next
    // false => res.send("báo sai token")
  } catch (err) {
    res.status(401).send(err);
  }
};

module.exports = {
  checkToken,
};

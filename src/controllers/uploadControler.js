const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    //   là nơi định nghĩa đường dẫn lưu
    cb(null, process.cwd() + "/public/img");
  },
  //   giúp bạn đổi tên file đang được up
  filename: (req, file, cb) => {
    // Math.random
    // time
    const newfileName = Data.now() + "-" + file.originalname; // cách ko bị trùng hình
    cb(null, newfileName);
  },
});

module.exports = multer({ storage });

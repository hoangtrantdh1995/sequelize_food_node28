const initModels = require("../models/init-models");
const sequelize = require("../models/index");

const model = initModels(sequelize);

// GET
const getFood = async (req, res) => {
  // food và food_type

  // let data = await model.food.findAll({
  //     include: ["type"] // chuỗi hoặc mảng chuỗi
  // });

  // order với user và food
  let data = await model.food.findAll({
    // include: ["food"],
  });

  res.send(data);
};

const uploadFood = (req, res) => {
  // localhost:8080/pulic/img + food.image
  const file = req.file;
  // file.filename

  // lưu ở dạng base64
  // lưu ở dạng: data:image/jpeg;base64, //mã hình  sadvsdvgdsgsgsfwihwjnlvds3r67858

  fs.readFile(process.cwd() + "/public/img/" + file.filename, (err, data) => {
    // base64 => chống lộ domain BE
    // let fileName = `"data:${req.file.mimetype};base64,${Buffer.from(data).toString("base64")}"`;

    let fileName = `data:${req.file.mimetype};base64,${Buffer.from(
      data.toString("base64")
    )}`;

    // xóa hình
    fs.unlinkSync(process.cwd() + "/public/img/" + file.filename);
    res.send(fileName);
  });

  res.send(file);
};

const likeRestaurent = async (req, res, next) => {
  try {
    const res_id = req.params.res_id;
    const user_id = res.locals.user.user_id;
    const like = await model.like_res.findOne({
      where: {
        res_id,
        user_id,
      },
    });
    if (like) {
      await model.like_res.destroy({
        where: {
          id: like.id,
        },
      });
    } else {
      await model.like_res.create({
        res_id,
        user_id,
        date_like: new Date(),
      });
    }
    res.status(200).json({
      message: "Like thành công",
    });
  } catch (err) {
    res.status(500).json({
      message: "Xóa like",
    });
  }
};

const getLikeRestaurent = async (req, res, next) => {
  try {
    const res_id = req.params.res_id;
    let data = await model.restaurant.findOne({
      where: {
        res_id,
      },
      include: {
        model: model.like_res,
        as: "like_res",
        include: ["user"],
      },
    });
    res.send(data);
  } catch (err) {
    res.status(500).json({
      message: "Lấy thất bại",
    });
  }
};




module.exports = {
  getFood,
  uploadFood,
  likeRestaurent,
  getLikeRestaurent,
};

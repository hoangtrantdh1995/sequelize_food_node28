// const User = require('../models/user');
const { Sequelize } = require("sequelize");
const { successCode, failCode, errorCode } = require("../config/response");
const bcrypt = require("bcrypt");
const initModels = require("../models/init-models");
const sequelize = require("../models/index");
const { generateToken } = require("../config/jwt");
const model = initModels(sequelize);
const Op = Sequelize.Op;

const getUser = async (req, res) => {
  try {
    let { id } = req.params;

    //  đồng bộ
    // findAll() => list object [{},{},{}]

    // SELECT * FROM user WHERE email LIKE '%a%';
    // let data = await User.findAll({
    //     where: {
    //         email: {
    //             [Op.like]: `%${id}%`
    //         }
    //     }
    // });

    // findOne() => object {}
    // let data2 = await User.find(1);
    // let data = await User.findAll();
    let data = await model.user.findAll();

    let a = 0;
    data.map((item) => {
      a = item.email;
    });

    // res.status(200).send(data);
    successCode(res, data, "Lấy dữ liệu thành công");

    //login
    // username sai, email sai, password sai

    // res.status(400).send("mật khẩu hoặc tên đăng nhập ko đúng");
  } catch (err) {
    // res.status(500).send("Lỗi BE");
    errorCode(res, "Lỗi BE");
  }
};

const createUser = async (req, res) => {
  try {
    //lấy từ FE
    let { full_name, email, pass_word } = req.body;

    let modelUser = {
      full_name,
      email,
      pass_word: bcrypt.hashSync(pass_word, 10),
    };

    //validate controller

    // thêm một dòng dữ liệu vào table
    await model.user.create(modelUser);

    // res.status(200).send("Tạo mới thành công");
    successCode(res, modelUser, "Tạo mới thành công");
  } catch (err) {
    console.log(err);
    // res.status(500).send("Lỗi BE");
    errorCode(res, "Lỗi BE");
  }
};

const updateUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let { full_name, email, pass_word } = req.body;

    let modelUser = {
      full_name,
      email,
      pass_word: bcrypt.hashSync(pass_word, 10),
    };

    await model.user.update(modelUser, {
      where: {
        user_id,
      },
    });

    //DETELE
    // User.destroy({where})

    // res.status(200).send("Update user");
    successCode(res, modelUser, "Update thành công");
  } catch (err) {
    // res.status(500).send("Lỗi BE");
    errorCode(res, "Lỗi BE");
  }
};

// yarn add bcrybt
// đăng ký => lấy thông tin user => mã password  => lưu xuống local
// login => kiểm tra email => kiểm tra password (hàm mã hóa)

const singUp = async (req, res) => {
  try {
    // lấy từ FE
    let { full_name, email, pass_word } = req.body;
    //phải parse data sang dạng json nha.
    let modelUser = {
      full_name,
      email,
      // mã hóa password
      pass_word: bcrypt.hashSync(pass_word, 10),
    };

    // []

    let checkEmail = await model.user.findOne({
      where: {
        email,
      },
    });

    // cách login facebook
    if (checkEmail) {
      // login thành công
      // kiểm tra password
      failCode(res, modelUser, "Mật khẩu không đúng !");
    } else {
      // valider
      // failCode(res, modelUser, "Email đã tồn tại !");
      await model.user.create(modelUser);

      successCode(res, modelUser, "Đăng ký thành công");
    }

    //login
    // username sai, email sai, password sai

    // res.status(400).send("mật khẩu hoặc tên đăng nhập ko đúng");
  } catch (err) {
    console.log("ERR", err);
    // res.status(500).send("Lỗi BE");
    errorCode(res, "Lỗi BE");
  }
};

const login = async (req, res) => {
  try {
    // lấy từ FE
    let { full_name, email, pass_word } = req.body;

    let modelUser = {
      full_name,
      email,
      pass_word,
    };

    // []

    let checkEmail = await model.user.findOne({
      where: {
        email,
      },
    });

    //B có đang thấy mình teamview ko ?? co ạ
    // Lỗi nãy là do b chưa import brypt vào á, mình mới test cái login, b thử sign up xem
    if (checkEmail) {
      // login thành công
      // kiểm tra password

      let checkPass = bcrypt.compareSync(pass_word, checkEmail.pass_word);

      if (checkPass) {
        // let data = {...checkEmail, pass_word: ""}
        let token = generateToken({
          data: { ...checkEmail.dataValues, pass_word: "" },
        });

        successCode(res, token, "Login thành công");
      } else {
        failCode(res, modelUser, "Mật khẩu không đúng !");
      }
    } else {
      // valider
      failCode(res, modelUser, "Email đã tồn tại !");
    }

    //login
    // username sai, email sai, password sai

    // res.status(400).send("mật khẩu hoặc tên đăng nhập ko đúng");
  } catch (err) {
    // res.status(500).send("Lỗi BE");
    console.log(err);
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  login,
  singUp,
  getUser,
  createUser,
  updateUser,
};

// yarn add jsonwebtoken => giải mã token
const jwt = require("jsonwebtoken");

// tạo token
const generateToken = (data) => {
  let token = jwt.sign(data, "private", { expiresIn: "10d" }); //giới hạn dùng etoken expiresIn
  return token;
};

generateToken({ title: "node 28" });

// kiểm tra token hợp lệ không
const verifyToken = (token) => {
  let checkToken = jwt.verify(token, "private");
  return checkToken;
};

const decodeToken = (token) => {
  let decode = jwt.decode(token);
  return decode;
};
module.exports = {
  verifyToken,
  decodeToken,
  generateToken,
};

// lệnh lấy token
//node src/config/jwt.js

//bearer token

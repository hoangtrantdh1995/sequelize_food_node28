// yarn add dotenv
require("dotenv").config();

// node src/config/index.js
// console.log(process.env.NODE_ENV); // => biến toàn cục

module.exports = {
  database: process.env.MYSQL_DATABASE,
  userName: process.env.MYSQL_USERNAME,
  passWord: process.env.MYSQL_PASSWORD,
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  dialect: process.env.MYSQL_DIALECT,
};

// kết nối CSDL
// host, database, user, password, port

const { Sequelize } = require("sequelize");
const config = require("../config/index");
console.log("config: ", config);

const sequelize = new Sequelize(
  config.database,
  config.userName,
  config.passWord,
  {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
  }
);
// báo lỗi connect
sequelize.authenticate().then(
  function () {
    console.log("DB connection sucessful.");
  },
  function (err) {
    // catch error here
    console.log(err);
  }
);
// sequelize.query("SELECT * FROM user");
module.exports = sequelize;

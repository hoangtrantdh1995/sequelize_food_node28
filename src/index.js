// yarn init -y
// yarn add express

// import thư viện express
const express = require("express");
// tạo biến gán cho hàm express
const app = express();

//middleware chuyển đổi để đọc định dạng json
app.use(express.json());
app.use(express.static(".")); // => định vị thư mục để load tài nguyên

// cho phép FE truy cập API từ BE
const cors = require("cors");
app.use(cors());

// tạo ra một host từ thư viện express
app.listen(8080); // port 8080

// MVC

//import rootRoute
const rootRoute = require("./routes/rootRoute");
app.use("/api", rootRoute);

// file system
const fs = require("fs");

console.log(__dirname); // => trả về đường dẫn file của bạn đang đứng
console.log(process.cwd()); // => trả về đường dẫn gốc của project

// tạo một file
fs.writeFile(process.cwd() + "/text.txt", "hello node 28", (err) => {});
// đọc file
fs.readFile(process.cwd() + "/text.txt", "utf-8", (err, data) => {
  console.log(data);
});
// xóa file
fs.unlink(process.cwd() + "/text.txt", (err) => {});

// khi lưu xuống database
//  localhost:8080/public/img/

// <img src = "${URL_BACKEND}${user.avatar}" />

// chỉ lưu tên hình xuống database
// img.jpeg

// yarn add multer

// swager
// http://localhost:8080/swagger/
// thư viện
// yarn add swagger-ui-express swagger-jsdoc
const swaggerUi = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

const options = {
  // định nghĩa option
  definition: {
    info: {
      title: "Swagger node 28",
      version: "1.0.0",
    },
  },
  // nơi định nghĩa API cho swager
  apis: ["src/swagger/index.js"],
};

const specs = swaggerJsDoc(options);

app.use("/swagger", swaggerUi.serve, swaggerUi.setup(specs));
